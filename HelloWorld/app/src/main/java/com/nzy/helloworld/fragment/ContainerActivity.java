package com.nzy.helloworld.fragment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nzy.helloworld.R;

public class ContainerActivity extends AppCompatActivity implements AFragment.IOnMessageClick {
    private AFragment aFragment;
    private BFragment bFragment;
    private Button myBtnChange;
    private TextView myTvTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        myBtnChange = findViewById(R.id.btn_change);
        myTvTitle = findViewById(R.id.tv_title);
        myBtnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( bFragment == null ) {
                    bFragment = new BFragment();
                }
                getFragmentManager().beginTransaction().replace(R.id.fl_container,bFragment).commitAllowingStateLoss();
            }
        });

        aFragment = AFragment.newInstance("哈哈哈");
        getFragmentManager().beginTransaction().add(R.id.fl_container,aFragment).commitAllowingStateLoss();
    }

    @Override
    public void onClick(String text) {
        myTvTitle.setText(text);
    }
}