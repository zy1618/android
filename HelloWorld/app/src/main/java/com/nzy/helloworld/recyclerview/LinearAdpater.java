package com.nzy.helloworld.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nzy.helloworld.R;

public class LinearAdpater extends RecyclerView.Adapter {
    private Context myContext;
    private OnItemClickListener myListener;
    public LinearAdpater(Context context,OnItemClickListener listener) {
        this.myContext = context;
        this.myListener = listener;
    }

    public interface OnItemClickListener {
        void onClick(int pos);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      if( viewType == 0 ) {
          return new LinearViewHolder(LayoutInflater.from(myContext).inflate(R.layout.layout_linear_item,parent,false));
      } else {
          return new LinearViewHolder2(LayoutInflater.from(myContext).inflate(R.layout.layout_linear_item2,parent,false));
      }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == 0) {
            ((LinearViewHolder)holder).textView.setText("Hello Wrold");
        } else {
            ((LinearViewHolder2)holder).textView.setText("Hello Wrold2");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Toast.makeText(myContext,"click..."+position, Toast.LENGTH_SHORT).show();*/
              myListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        if( position % 2 == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return 30;
    }

    class LinearViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        public LinearViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_title);
        }
    }

    class LinearViewHolder2 extends RecyclerView.ViewHolder {
        private TextView textView;
        private ImageView ig;
        public LinearViewHolder2(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_title);
            ig = itemView.findViewById(R.id.iv_image);
        }
    }
}
