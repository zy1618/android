package com.nzy.helloworld.recyclerview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Rect;
import android.os.Bundle;
import android.widget.Toast;

import com.nzy.helloworld.R;

public class LinearRecyclerViewActivity extends AppCompatActivity {
    private RecyclerView myRvMain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_recycler_view);
        myRvMain = findViewById(R.id.rv_main);
        myRvMain.setLayoutManager(new LinearLayoutManager(LinearRecyclerViewActivity.this));
        myRvMain.setAdapter(new LinearAdpater(LinearRecyclerViewActivity.this, new LinearAdpater.OnItemClickListener() {
            @Override
            public void onClick(int pos) {
                Toast.makeText(LinearRecyclerViewActivity.this,"postion"+ pos,Toast.LENGTH_SHORT).show();
            }
        }));
     }


}