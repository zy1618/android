package com.nzy.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.nzy.helloworld.util.ToastUtil;

public class HandlerActivity extends AppCompatActivity {
    private Handler myHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler);
        /*myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(),ButtonActivity.class);
                startActivity(intent);
            }
        },3000);*/
        myHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                switch(msg.what) {
                    case 1:
                        ToastUtil.showMsg(getApplicationContext(),"线程通信成功");
                        break;
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                super.run();
                Message message = new Message();
                message.what=1;
                myHandler.sendMessage(message);
            }
        };
    }
}