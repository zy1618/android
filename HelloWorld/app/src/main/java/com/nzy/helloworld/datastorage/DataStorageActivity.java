package com.nzy.helloworld.datastorage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.nzy.helloworld.R;

public class DataStorageActivity extends AppCompatActivity implements View.OnClickListener {
    private Button myBtnShare,myBtnFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_storage);
        myBtnShare = findViewById(R.id.btn_sharedpreferences);
        myBtnFile = findViewById(R.id.btn_file);
        myBtnShare.setOnClickListener(this);
        myBtnFile.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btn_sharedpreferences:
                intent = new Intent(getApplicationContext(),SharedPreferencesActivity.class);
                break;
            case R.id.btn_file:
                intent = new Intent(getApplicationContext(),FileActivity.class);
                break;
        }
        startActivity(intent);
    }
}