package com.nzy.helloworld.anim;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.nzy.helloworld.R;

public class AnimateActivity extends AppCompatActivity {
    private Button myBtn1,myBtn2,myBtn3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animate);
        myBtn1 = findViewById(R.id.btn1);
        myBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ObjectAnimActivity.class);
                startActivity(intent);
            }
        });
    }
}