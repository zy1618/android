package com.nzy.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nzy.helloworld.fragment.ContainerActivity;
import com.nzy.helloworld.gridview.GridViewActivity;
import com.nzy.helloworld.jump.AActivity;
import com.nzy.helloworld.recyclerview.RecyclerViewActivity;

public class ButtonActivity extends AppCompatActivity {
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btn10;
    private Button btn12;
    private Button btn13;
    private Button btn14;
    private Button btn15;
    private Button btn16;
    private Button btn17;
    private Button btn18;
    private Button btn19;
    private Button btn20;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);
        btn5 = findViewById(R.id.btn5);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ButtonActivity.this,"被点击",Toast.LENGTH_SHORT).show();
            }
        });
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);
        btn10 = findViewById(R.id.btn10);
        btn12 = findViewById(R.id.btn12);
        btn13 = findViewById(R.id.btn13);
        btn14 = findViewById(R.id.btn14);
        btn15 = findViewById(R.id.btn15);
        btn16 = findViewById(R.id.btn16);
        btn17 = findViewById(R.id.btn17);
        btn18 = findViewById(R.id.btn18);
        btn19 = findViewById(R.id.btn19);
        btn20 = findViewById(R.id.btn20);
        setListener();
    }
    private void setListener() {
        OnClick onClick = new OnClick();
        btn6.setOnClickListener(onClick);
        btn7.setOnClickListener(onClick);
        btn8.setOnClickListener(onClick);
        btn9.setOnClickListener(onClick);
        btn10.setOnClickListener(onClick);
        btn12.setOnClickListener(onClick);
        btn13.setOnClickListener(onClick);
        btn14.setOnClickListener(onClick);
        btn15.setOnClickListener(onClick);
        btn16.setOnClickListener(onClick);
        btn17.setOnClickListener(onClick);
        btn18.setOnClickListener(onClick);
        btn19.setOnClickListener(onClick);
        btn20.setOnClickListener(onClick);
    }
    private class OnClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = null;
            switch(v.getId()) {
                case R.id.btn6:
                    intent = new Intent(ButtonActivity.this,EditTextActivity.class);
                    break;
                case R.id.btn7:
                    intent = new Intent(ButtonActivity.this,RadioButtonActivity.class);
                    break;
                case R.id.btn8:
                    intent = new Intent(ButtonActivity.this,CheckBoxActivity.class);
                    break;
                case R.id.btn9:
                    intent = new Intent(ButtonActivity.this,ImageViewActivity.class);
                    break;
                case R.id.btn10:
                    intent = new Intent(ButtonActivity.this, GridViewActivity.class);
                    break;
                case R.id.btn12:
                    intent  = new Intent(ButtonActivity.this, RecyclerViewActivity.class);
                    break;
                case R.id.btn13:
                    intent  = new Intent(ButtonActivity.this, WebViewActivity.class);
                    break;
                case R.id.btn14:
                    intent  = new Intent(ButtonActivity.this, ToastActivity.class);
                    break;
                case R.id.btn15:
                    intent  = new Intent(ButtonActivity.this, DialogActivity.class);
                    break;
                case R.id.btn16:
                    intent  = new Intent(ButtonActivity.this, ProgressActivity.class);
                    break;
                case R.id.btn17:
                    intent  = new Intent(ButtonActivity.this, CustomDialogActivity.class);
                    break;
                case R.id.btn18:
                    intent  = new Intent(ButtonActivity.this, PopupWindowActivity.class);
                    break;
                case R.id.btn19:
                    intent  = new Intent(ButtonActivity.this, AActivity.class);
                    break;
                case R.id.btn20:
                    intent  = new Intent(ButtonActivity.this, ContainerActivity.class);
                    break;
            }
            startActivity(intent);
        }
    }
}
