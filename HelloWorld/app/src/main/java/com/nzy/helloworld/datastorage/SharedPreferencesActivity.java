package com.nzy.helloworld.datastorage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nzy.helloworld.R;

public class SharedPreferencesActivity extends AppCompatActivity {
    private EditText myEtName;
    private Button myBtnSave,myBtnShow;
    private TextView myTvShow;
    private SharedPreferences mySharedPreferences;
    private SharedPreferences.Editor myEditor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);
        myEtName = findViewById(R.id.et_name);
        myBtnSave = findViewById(R.id.btn_save);
        myBtnShow = findViewById(R.id.btn_show);
        myTvShow = findViewById(R.id.tv_show);
        mySharedPreferences = getSharedPreferences("data",MODE_PRIVATE);
        myEditor = mySharedPreferences.edit();
        myBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myEditor.putString("name",myEtName.getText().toString());
                myEditor.apply();
            }
        });
        myBtnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTvShow.setText(mySharedPreferences.getString("name",""));
            }
        });
    }
}