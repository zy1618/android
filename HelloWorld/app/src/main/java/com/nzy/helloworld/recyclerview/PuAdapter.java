package com.nzy.helloworld.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nzy.helloworld.R;

public class PuAdapter extends RecyclerView.Adapter<PuAdapter.PuViewHolder> {
    private Context myContext;
    public PuAdapter(Context context) {
        this.myContext = context;
    }
    @NonNull
    @Override
    public PuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PuViewHolder(LayoutInflater.from(myContext).inflate(R.layout.layout_pu_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull PuViewHolder holder, int position) {
        if(position % 2 != 0) {
            holder.ig.setImageResource(R.drawable.bg);
        } else {
            holder.ig.setImageResource(R.drawable.cb);
        }
    }

    @Override
    public int getItemCount() {
        return 50;
    }

    class PuViewHolder extends RecyclerView.ViewHolder {
        private ImageView ig;
        public PuViewHolder(View itemView) {
            super(itemView);
            ig = itemView.findViewById(R.id.ig1);
        }
    }
}
