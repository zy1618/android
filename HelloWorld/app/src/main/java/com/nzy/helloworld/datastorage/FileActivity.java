package com.nzy.helloworld.datastorage;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nzy.helloworld.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileActivity extends AppCompatActivity {
    private EditText myEtName;
    private Button myBtnSave,myBtnShow;
    private TextView myTvShow;
    private final String myFileName = "test.txt";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);
        myEtName = findViewById(R.id.et_name);
        myBtnSave = findViewById(R.id.btn_save);
        myBtnShow = findViewById(R.id.btn_show);
        myTvShow = findViewById(R.id.tv_show);

        myBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(myEtName.getText().toString());
            }
        });
        myBtnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTvShow.setText(read());
            }
        });
    }
    private void save(String content) {
        FileOutputStream fileOutputStream = null;
        try {
            File dir = new File(Environment.getExternalStorageDirectory(),"skapan");
            if(!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(dir,myFileName);
            if(!file.exists()) {
                file.createNewFile();
            }
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(content.getBytes());
            System.out.println(content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("已经存好了");
            if(fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private String read() {
        FileInputStream fileInputStream = null;
        try {
            File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile().getAbsolutePath() + File.separator +"skapan",myFileName);
            fileInputStream = new FileInputStream(file);
            byte[] buff = new byte[1024];
            StringBuilder sb = new StringBuilder("");
            int len = 0;
            while((len = fileInputStream.read(buff)) > 0) {
                sb.append(new String(buff,0,len));
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }
}