package com.nzy.helloworld.broadcast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nzy.helloworld.R;

public class BroadActivity extends AppCompatActivity {
    private Button myBtn1;
    private TextView myTvTest;
    private MyBroadcast myBroadcast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broad1);
        myBtn1 = findViewById(R.id.btn1);
        myTvTest = findViewById(R.id.tv_test);
        myBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getApplicationContext(),BroadActivity2.class);
                startActivity(intent);
            }
        });
        myBroadcast = new MyBroadcast();
        IntentFilter intentFilter =new IntentFilter();
        intentFilter.addAction("com.skyspan.update");
        LocalBroadcastManager.getInstance(this).registerReceiver(myBroadcast,intentFilter);
    }



    private class MyBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch(intent.getAction()) {
                case "com.skypan.update":
                    myTvTest.setText("123");
                    break;
            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myBroadcast);
    }
}