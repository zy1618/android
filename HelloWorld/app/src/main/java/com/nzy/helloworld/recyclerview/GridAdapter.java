package com.nzy.helloworld.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nzy.helloworld.R;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.GridViewHolder>  {
    private Context myContext;
    private OnItemClickListener myListener;
    public GridAdapter(Context context,OnItemClickListener listener) {
        this.myContext = context;
        this.myListener = listener;
    };
    @NonNull
    @Override
    public GridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GridViewHolder(LayoutInflater.from(myContext).inflate(R.layout.layout_grid_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull GridViewHolder holder, int position) {
        holder.textView.setText("H");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 100;
    }

    class GridViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        public GridViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.tv_title);
        }
    }

    public interface OnItemClickListener {
        void onClick(int pos);
    }
}
