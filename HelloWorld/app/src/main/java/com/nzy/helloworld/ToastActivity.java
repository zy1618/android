package com.nzy.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nzy.helloworld.R;
import com.nzy.helloworld.util.ToastUtil;

public class ToastActivity extends AppCompatActivity {
    private Button myBtnToast1,myBtnToast2,myBtnToast3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);
        myBtnToast1 = findViewById(R.id.btn_toast_1);
        myBtnToast2 = findViewById(R.id.btn_toast_2);
        myBtnToast3 = findViewById(R.id.btn_toast_3);
        OnClick click = new OnClick();
        myBtnToast1.setOnClickListener(click);
        myBtnToast2.setOnClickListener(click);
        myBtnToast3.setOnClickListener(click);
    }

    class OnClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.btn_toast_1:
                    Toast.makeText(getApplicationContext(),"Toast",Toast.LENGTH_SHORT).show();
                    break;
                case R.id.btn_toast_2:
                   Toast toastCenter = Toast.makeText(getApplicationContext(),"Toast",Toast.LENGTH_SHORT);
                    toastCenter.setGravity(Gravity.CENTER,0,0);
                    toastCenter.show();
                    break;
                case R.id.btn_toast_3:
                    ToastUtil.showMsg(getApplicationContext(),"包装过的Toast");
                    break;
            }
        }
    }
}