package com.nzy.helloworld;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.nzy.helloworld.util.ToastUtil;

public class DialogActivity extends AppCompatActivity {
    private Button myBtnDialog1,myBtnDialog2,myBtnDialog3,myBtnDialog4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        myBtnDialog1 = findViewById(R.id.btn_dialog1);
        myBtnDialog2 = findViewById(R.id.btn_dialog2);
        myBtnDialog3 = findViewById(R.id.btn_dialog3);
        myBtnDialog4 = findViewById(R.id.btn_dialog4);
        OnClick onclick = new OnClick();
        myBtnDialog1.setOnClickListener(onclick);
        myBtnDialog2.setOnClickListener(onclick);
        myBtnDialog3.setOnClickListener(onclick);
        myBtnDialog4.setOnClickListener(onclick);
    }

    class OnClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            switch(v.getId()) {
                case R.id.btn_dialog1:
                    AlertDialog.Builder builder = new AlertDialog.Builder(DialogActivity.this);
                    builder.setTitle("请回答").setMessage("你觉得课程如何?")
                            .setPositiveButton("棒", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ToastUtil.showMsg(DialogActivity.this,"非常好");
                                }
                            }).setNeutralButton("还行", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ToastUtil.showMsg(DialogActivity.this,"哈哈哈");
                        }
                    }).setNegativeButton("不行", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ToastUtil.showMsg(DialogActivity.this,"呵呵");
                        }
                    }).show();
                    break;
                case R.id.btn_dialog2:
                    final String[] strArr = new String[]{"男","女"};
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DialogActivity.this);
                    builder1.setTitle("请选择性别").setItems(strArr, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ToastUtil.showMsg(DialogActivity.this,strArr[which]);
                        }
                    }).show();
                case R.id.btn_dialog3:
                    final String[] strArr1 = new String[]{"男","女"};
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(DialogActivity.this);
                    builder2.setTitle("请选择性别").setSingleChoiceItems(strArr1,1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ToastUtil.showMsg(DialogActivity.this,strArr1[which]);
                            dialog.dismiss();
                        }
                    }).setCancelable(false).show();
                case R.id.btn_dialog4:
                    final String[] strArr2 = new String[]{"唱歌","跳舞","写代码"};
                    boolean[] isSelected = new boolean[]{false,true,false};
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(DialogActivity.this);
                    builder3.setTitle("选择兴趣").setMultiChoiceItems(strArr2, isSelected, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                            ToastUtil.showMsg(DialogActivity.this,strArr2[which]+":"+isChecked);
                        }
                    }).show();
            }
        }
    }
}