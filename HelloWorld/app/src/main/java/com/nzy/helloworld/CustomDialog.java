package com.nzy.helloworld;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class CustomDialog extends Dialog implements View.OnClickListener{


    private TextView myTvTitle,myTvMessage,myTvCancel,myTvConfirm;
    private String title,message,cancel,confirm;
    private IOnCancelListener cancelListener;
    private IOnConfirmListener confirmListener;

    public CustomDialog setTitle(String title) {
        this.title = title;
        return this;
    }

    public CustomDialog setMessage(String message) {
        this.message = message;
        return this;
    }

    public CustomDialog setCancel(String cancel,IOnCancelListener listener) {
        this.cancel = cancel;
        this.cancelListener = listener;
        return this;
    }

    public CustomDialog setConfirm(String confirm,IOnConfirmListener listener) {
        this.confirm = confirm;
        this.confirmListener = listener;
        return this;
    }

    public CustomDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_custom);
        myTvTitle = findViewById(R.id.tv_title);
        myTvMessage = findViewById(R.id.tv_message);
        myTvCancel = findViewById(R.id.tv_cancel);
        myTvConfirm = findViewById(R.id.tv_confirm);
        if(!TextUtils.isEmpty(message)) {
            myTvMessage.setText(message);
        }
        if(!TextUtils.isEmpty(title)) {
            myTvTitle.setText(title);
        }
        if(!TextUtils.isEmpty(cancel)) {
            myTvCancel.setText(cancel);
        }
        if(!TextUtils.isEmpty(confirm)) {
            myTvConfirm.setText(confirm);
        }
        myTvConfirm.setOnClickListener(this);
        myTvCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                if(cancelListener != null) {
                    cancelListener.onCancel(this);
                }
                break;
            case R.id.tv_confirm:
                if(confirmListener != null) {
                    confirmListener.onConfirm(this);
                }
        }
    }

    public interface IOnCancelListener {
        void onCancel(CustomDialog dialog);
    }
    public interface IOnConfirmListener {
        void onConfirm(CustomDialog dialog);
    }
}
