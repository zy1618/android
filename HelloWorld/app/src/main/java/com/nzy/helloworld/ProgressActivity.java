package com.nzy.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.Touch;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.nzy.helloworld.util.ToastUtil;

import java.util.logging.LogRecord;

public class ProgressActivity extends AppCompatActivity {
    private ProgressBar myPb3;

    private Button myBtnStart,myBtnDl1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);
        myPb3 = findViewById(R.id.pb3);
        myBtnStart = findViewById(R.id.btn_start);
        myBtnDl1 = findViewById(R.id.btn_progress_dialog1);
        myBtnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.sendEmptyMessage(0);
            }
        });
        myBtnDl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog(ProgressActivity.this);
                progressDialog.setTitle("提示");
                progressDialog.setMessage("正在加载");
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        ToastUtil.showMsg(ProgressActivity.this,"cancel");
                    }
                });
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        });
    }
    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(myPb3.getProgress() < 100) {
                handler.postDelayed(runnable,500);
            } else {
                ToastUtil.showMsg(ProgressActivity.this,"加载完成");
            }
        }
    };
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            myPb3.setProgress(myPb3.getProgress() + 5);
            handler.sendEmptyMessage(0);
        }
    };
}