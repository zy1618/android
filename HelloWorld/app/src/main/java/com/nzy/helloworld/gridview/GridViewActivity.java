package com.nzy.helloworld.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;

import com.nzy.helloworld.R;

public class GridViewActivity extends AppCompatActivity {

    private GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);
        gridView = findViewById(R.id.gv1);
        gridView.setAdapter(new GridViewAdapter(GridViewActivity.this));
        System.out.println(1);
    }
}