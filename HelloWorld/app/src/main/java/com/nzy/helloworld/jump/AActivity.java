package com.nzy.helloworld.jump;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nzy.helloworld.R;
import com.nzy.helloworld.util.ToastUtil;

public class AActivity extends AppCompatActivity {
    private Button myBtnIt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);
        myBtnIt = findViewById(R.id.btn_it);
        myBtnIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),BActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("name","haha");
                bundle.putString("pwd","hehe");
                intent.putExtras(bundle);
                startActivityForResult(intent,0);
            }
            
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(AActivity.this,data.getExtras().getString("title"), Toast.LENGTH_SHORT).show();
    }
}