package com.nzy.helloworld.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.nzy.helloworld.R;

public class HorRecyclerViewActivity extends AppCompatActivity {
    private RecyclerView myRh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hor_recycler_view);
        myRh = findViewById(R.id.rv_hor);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HorRecyclerViewActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        myRh.setLayoutManager(linearLayoutManager);
        myRh.setAdapter(new HorRecyclerAdapter(HorRecyclerViewActivity.this));
    }
}