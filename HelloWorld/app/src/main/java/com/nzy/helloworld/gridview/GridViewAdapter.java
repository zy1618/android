package com.nzy.helloworld.gridview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nzy.helloworld.R;

public class GridViewAdapter extends BaseAdapter {
    private Context myContext;
    private LayoutInflater myLayoutInflater;

    public GridViewAdapter(Context context) {
        this.myContext = context;
        this.myLayoutInflater = LayoutInflater.from(context);
        System.out.println(1);
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolder {
        public ImageView imageView;
        public TextView textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println(2);
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = myLayoutInflater.inflate(R.layout.activity_grid_view_item, null);
            holder = new ViewHolder();
            holder.imageView = convertView.findViewById(R.id.iv1);
            holder.textView = convertView.findViewById(R.id.tv1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        System.out.println(4);
        holder.textView.setText("花");
        Glide.with(myContext).load("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png").into(holder.imageView);
        return convertView;
    }
}
