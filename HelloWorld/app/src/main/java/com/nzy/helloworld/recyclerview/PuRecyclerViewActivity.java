package com.nzy.helloworld.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;

import com.nzy.helloworld.R;

public class PuRecyclerViewActivity extends AppCompatActivity {
    private RecyclerView myPu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pu_recycler_view);
        myPu = findViewById(R.id.rv_pu);
        myPu.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        myPu.setAdapter(new PuAdapter(PuRecyclerViewActivity.this));
    }
}