package com.nzy.helloworld.anim;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.nzy.helloworld.R;

public class ObjectAnimActivity extends AppCompatActivity {
    private TextView myTv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_anim);
        myTv = findViewById(R.id.tv);

     /*   myTv.animate().translationYBy(500).setDuration(2000).start();  移动*/
        /*myTv.animate().alpha(0).setDuration(2000).start();  渐变色*/

        ValueAnimator valueAnimator = ValueAnimator.ofInt(0,100);
        valueAnimator.setDuration(2000);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Log.d("aaaa",animation.getAnimatedValue()+"");
                Log.d("aaaa",animation.getAnimatedFraction()+"");
            }
        });
        valueAnimator.start();


       /* ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(myTv,"translationY",0,500);
        objectAnimator.setDuration(2000);
        objectAnimator.start();*/
    }
}