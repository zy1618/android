package com.nzy.helloworld.fragment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nzy.helloworld.R;

public class AFragment extends Fragment {
    private TextView myTvTitle;
    private Button myBtnMsg;
    private IOnMessageClick listener;
    public static AFragment newInstance(String title) {
        AFragment fragment = new AFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        fragment.setArguments(bundle);
        return fragment;
    }
    public interface  IOnMessageClick {
        void onClick(String text);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_a,container,false);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (IOnMessageClick) context;
        }catch (ClassCastException e) {
            throw new ClassCastException("Activity必须实现接口");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myTvTitle = view.findViewById(R.id.tv_title);
        myBtnMsg = view.findViewById(R.id.btn_message);
        if(getArguments() != null ) {
            myTvTitle.setText(getArguments().getString("title"));
        }
        myBtnMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick("你好");
            }
        });
    }
}

